<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['BE_MOD'], 1, array('simple_recipes' => array()));

$GLOBALS['BE_MOD']['simple_recipes']['simple_recipes_categories'] = array
(
    'tables' => array('tl_simple_recipes_categories'),
    'stylesheet' => 'system/modules/simple_recipes/assets/css/be.css',
    'icon'   => 'system/modules/simple_recipes/assets/icons/category.png'
);

$GLOBALS['BE_MOD']['simple_recipes']['simple_recipes_entries'] = array
(
    'tables' => array('tl_simple_recipes_entries','tl_simple_recipes_properties'),
    'stylesheet' => 'system/modules/simple_recipes/assets/css/be.css',
    'icon'   => 'system/modules/simple_recipes/assets/icons/cutlery.png'
);




/**
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['FE_MOD'], 2, array
(
    'Recipes' => array
    (
        'simple_recipes_list'    => 'Srhinow\SimpleRecipes\Modules\Frontend\ModuleSimpleRecipesList',
        'simple_recipes_details'    => 'Srhinow\SimpleRecipes\Modules\Frontend\ModuleSimpleRecipesDetails',
    )
));

$GLOBALS['TL_MODELS']['tl_simple_recipes_entries'] = \Srhinow\SimpleRecipes\Models\SimpleRecipesEntriesModel::class;
$GLOBALS['TL_MODELS']['tl_simple_recipes_properties'] = \Srhinow\SimpleRecipes\Models\SimpleRecipesPropertiesModel::class;

/**
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 */
// $GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('mpMembers', 'changeIAOTags');
$GLOBALS['TL_HOOKS']['getSearchablePages'][] = array('Srhinow\SimpleRecipes\Hooks\SimpleRecipesHooks', 'getSearchablePages');

/**
 * -------------------------------------------------------------------------
 * Permissions are access settings for user and groups (fields in tl_user and tl_user_group)
 * -------------------------------------------------------------------------
 */
// $GLOBALS['TL_PERMISSIONS'][] = 'simple_recipes_modules';
$GLOBALS['BM']['PROPERTIES']['ID'] = 1;

