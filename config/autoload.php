<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2018 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'Srhinow',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Library
	'Srhinow\SimpleRecipes\Hooks\SimpleRecipesHooks'                    => 'system/modules/simple_recipes/library/Hooks/SimpleRecipesHooks.php',

	// Modules
	'Srhinow\SimpleRecipes\Modules\Frontend\ModuleSimpleRecipes'        => 'system/modules/simple_recipes/modules/Frontend/ModuleSimpleRecipes.php',
	'Srhinow\SimpleRecipes\Modules\Frontend\ModuleSimpleRecipesList'    => 'system/modules/simple_recipes/modules/Frontend/ModuleSimpleRecipesList.php',
	'Srhinow\SimpleRecipes\Modules\Frontend\ModuleSimpleRecipesDetails' => 'system/modules/simple_recipes/modules/Frontend/ModuleSimpleRecipesDetails.php',

	// Models
	'Srhinow\SimpleRecipes\Models\SimpleRecipesCategoriesModel'         => 'system/modules/simple_recipes/models/SimpleRecipesCategoriesModel.php',
	'Srhinow\SimpleRecipes\Models\SimpleRecipesPropertiesModel'         => 'system/modules/simple_recipes/models/SimpleRecipesPropertiesModel.php',
	'Srhinow\SimpleRecipes\Models\SimpleRecipesEntriesModel'            => 'system/modules/simple_recipes/models/SimpleRecipesEntriesModel.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'mod_simple_recipes_entries_empty' => 'system/modules/simple_recipes/templates/modules',
	'mod_simple_recipes_list'          => 'system/modules/simple_recipes/templates/modules',
	'mod_simple_recipes_details'       => 'system/modules/simple_recipes/templates/modules',
	'mod_simple_recipes_details_alt'   => 'system/modules/simple_recipes/templates/modules',
	'item_simple_recipes_archivelist'  => 'system/modules/simple_recipes/templates/items',
	'item_simple_recipes_details'      => 'system/modules/simple_recipes/templates/items',
	'item_simple_recipes_list'         => 'system/modules/simple_recipes/templates/items',
));
