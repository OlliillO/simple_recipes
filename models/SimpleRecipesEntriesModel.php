<?php
namespace Srhinow\SimpleRecipes\Models;

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */


use Contao\Model;

/**
 * Reads and writes Credit Items
 *
 * @property integer $id
 * @property integer $tstamp
 * @property string  $title
 *
 * @method static SimpleRecipesEntriesModel|null findById($id, $opt=array())
 * @method static SimpleRecipesEntriesModel|null findByIdOrAlias($val, $opt=array())
 * @method static SimpleRecipesEntriesModel|null findByPk($id, $opt=array())
 * @method static SimpleRecipesEntriesModel|null findOneBy($col, $val, $opt=array())
 * @method static SimpleRecipesEntriesModel|null findOneByTstamp($val, $opt=array())
 * @method static SimpleRecipesEntriesModel|null findOneByTitle($val, $opt=array())

 *
 * @method static \Model\Collection|SimpleRecipesEntriesModel[]|SimpleRecipesEntriesModel|null findByTstamp($val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesEntriesModel[]|SimpleRecipesEntriesModel|null findByTitle($val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesEntriesModel[]|SimpleRecipesEntriesModel|null findBy($col, $val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesEntriesModel[]|SimpleRecipesEntriesModel|null findAll($opt=array())
 *
 * @method static integer countById($id, $opt=array())
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */


class SimpleRecipesEntriesModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_simple_recipes_entries';

	/**
	 * Find all active and published recipe items
	 *
	 * @param integer $intPid     The news archive ID
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function findRecipes($intLimit=0, $intOffset=0, array $arrIds=array(), array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;

		if(is_array($arrIds) && count($arrIds) > 0)
		{
			$arrColumns = array("$t.id IN(" . implode(',', array_map('intval', $arrIds)) . ")");
		}

        if (!BE_USER_LOGGED_IN)
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1'";
        }

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order'] = "$t.headline ASC";
		}

		$arrOptions['limit']  = $intLimit;
		$arrOptions['offset'] = $intOffset;


		return static::findBy($arrColumns, null, $arrOptions);
	}

    /**
     * Find all published recipe items from the past to now
     *
     * @param integer $intPid     The news archive ID
     * @param array   $arrOptions An optional options array
     *
     * @return \Model\Collection|null A collection of models or null if there are no news
     */
    public static function findArchiveRecipes($intLimit=0, $intOffset=0, array $arrIds=array(), array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = null;

        if(is_array($arrIds) && count($arrIds) > 0)
        {
            $arrColumns = array("$t.id IN(" . implode(',', array_map('intval', $arrIds)) . ")");
        }

        if (!BE_USER_LOGGED_IN)
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = " ($t.start='' OR $t.start <= '" . ($time) . "') AND $t.published='1'";
        }

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order'] = "$t.start DESC";
        }

        $arrOptions['limit']  = $intLimit;
        $arrOptions['offset'] = $intOffset;


        return static::findBy($arrColumns, null, $arrOptions);
    }
	/**
	 * Count all recipe items
	 *
	 * @param integer $intPid     The news archive ID
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function countRecipesByOpen(array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;

        if (!BE_USER_LOGGED_IN)
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1'";
        }

		return static::countBy($arrColumns, null, $arrOptions);
	}

    /**
     * Count all recipe items
     *
     * @param integer $intPid     The news archive ID
     * @param array   $arrOptions An optional options array
     *
     * @return \Model\Collection|null A collection of models or null if there are no news
     */
    public static function countArchiveRecipesByOpen(array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = null;

        if (!BE_USER_LOGGED_IN)
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = " ($t.start='' OR $t.start <= '" . ($time) . "') AND $t.published='1'";
        }

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
