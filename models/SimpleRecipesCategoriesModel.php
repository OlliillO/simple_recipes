<?php
namespace Srhinow\SimpleRecipes\Models;

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

use Contao\Model;

/**
 * Reads and writes Credit Items
 *
 * @property integer $id
 * @property integer $tstamp
 * @property string  $title
 *
 * @method static SimpleRecipesCategoriesModel|null findById($id, $opt=array())
 * @method static SimpleRecipesCategoriesModel|null findByIdOrAlias($val, $opt=array())
 * @method static SimpleRecipesCategoriesModel|null findByPk($id, $opt=array())
 * @method static SimpleRecipesCategoriesModel|null findOneBy($col, $val, $opt=array())
 * @method static SimpleRecipesCategoriesModel|null findOneByTstamp($val, $opt=array())
 * @method static SimpleRecipesCategoriesModel|null findOneByTitle($val, $opt=array())

 *
 * @method static \Model\Collection|SimpleRecipesCategoriesModel[]|SimpleRecipesCategoriesModel|null findByTstamp($val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesCategoriesModel[]|SimpleRecipesCategoriesModel|null findByTitle($val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesCategoriesModel[]|SimpleRecipesCategoriesModel|null findBy($col, $val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesCategoriesModel[]|SimpleRecipesCategoriesModel|null findAll($opt=array())
 *
 * @method static integer countById($id, $opt=array())
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */

class SimpleRecipesCategoriesModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_simple_recipes_categories';

	/**
	 * Find all open recipe items
	 *
	 * @param integer $intPid     The news archive ID
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function findRecipes($intLimit=0, $intOffset=0, array $arrIds=array(), array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;

		if(is_array($arrIds) && count($arrIds) > 0)
		{
			$arrColumns = array("$t.id IN(" . implode(',', array_map('intval', $arrIds)) . ")");
		}

		$arrColumns[] = "$t.open='1'";

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order'] = "$t.headline ASC";
		}

		$arrOptions['limit']  = $intLimit;
		$arrOptions['offset'] = $intOffset;


		return static::findBy($arrColumns, null, $arrOptions);
	}
	/**
	 * Count all recipe items
	 *
	 * @param integer $intPid     The news archive ID
	 * @param array   $arrOptions An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function countRecipesByOpen(array $arrOptions=array())
	{
		$t = static::$strTable;
		$arrColumns = null;

		if (!BE_USER_LOGGED_IN)
		{
			$arrColumns[] = "$t.open='1'";
		}

		return static::countBy($arrColumns, null, $arrOptions);
	}
}
