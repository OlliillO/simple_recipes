<?php

/**
 * PHP version 5
 * @copyright  sr-tag.de 2015
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

/**
 * Table tl_simple_recipes_entries
 */
$GLOBALS['TL_DCA']['tl_simple_recipes_entries'] = array
(
	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		// 'switchToEdit'                => true,
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
                'alias' => 'index',
                'start,stop,published' => 'index'
			)
		)
	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
            'fields'                  => array('start'),
			'flag'                    => 8,
            'panelLayout'             => 'filter;sort,search,limit',
		),
		'label' => array
		(
			'fields'                  => array('headline','start','stop'),
            'label_callback'          => array('tl_simple_recipes_entries', 'listEntries'),
		),
		'global_operations' => array
		(
			'properties' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['properties'],
				'href'                => 'table=tl_simple_recipes_properties&act=edit&id=1',
				'class'               => 'properties',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();"',
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif',
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif',
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"',
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif',
			)
		)
	),
	// Palettes
	'palettes' => array
	(
		'__selector__'                => array('addImage'),
		'default' => '{meta_legend},categories,difficulty,number_of_people;{recipes_text},headline,alias,main_ingredient,ingredients,preparation;{image_legend},addImage;{general_legend},published,start,stop'
	),

	// Subpalettes
	'subpalettes' => array
	(
		'addImage'                    => 'singleSRC,alt,title',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'headline' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['headline'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255,'tl_class'=>'long'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'main_ingredient' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['main_ingredient'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'alias' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['alias'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'alnum', 'doNotCopy'=>true, 'spaceToUnderscore'=>true, 'maxlength'=>128),
			'sql'					=> "varchar(64) NOT NULL default ''",
			'save_callback' => array
			(
				array('tl_simple_recipes_entries', 'generateAlias')
			)

		),
		'categories' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['categories'],
			'exclude'                 => true,
			'filter'                  => true,
			'search'                  => true,
			'flag'                    => 1,
			'inputType'               => 'checkboxWizard',
			'foreignKey'              => 'tl_simple_recipes_categories.title',
			'eval'                    => array('multiple'=>true),
			'sql'                     => "blob NULL",
			'relation'                => array('type'=>'belongsToMany', 'load'=>'lazy')
		),
		'difficulty' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['difficulty'],
			'exclude'                 => true,
			'inputType'               => 'select',
			'default'				  => 3,
			'options'                 => $GLOBALS['TL_LANG']['tl_simple_recipes_entries']['difficulty_options'],
			'eval'                    => array('mandatory'=>true, 'feEditable'=>true, 'feViewable'=>true, 'tl_class'=>'clr w50'),
			'sql'                     => "varchar(25) NOT NULL default ''"
		),
		'number_of_people' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['number_of_people'],
			'exclude'                 => false,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'natural', 'maxlength'=>8, 'tl_class'=>'w50'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'ingredients' => array
		(
			'label'			=> &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['ingredients'],
			'exclude' 		=> true,
			'inputType' 	=> 'multiColumnWizard',
			'eval' 			=> array
			(
				'columnFields' => array
				(
					'count' => array
					(
						'label'                 => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['count'],
						'exclude'               => true,
						'inputType'             => 'text',
						'eval' 					=> array('mandatory'=>false,'style'=>'width:60px')
					),
					'unit' => array
					(
						'label'                 => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['unit'],
						'exclude'               => true,
						'inputType'             => 'text',
						'eval' 					=> array('mandatory'=>false,'style'=>'width:120px')
					),
					'ingredient' => array
					(
						'label'                 => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['ingredient'],
						'exclude'               => true,
						'inputType'             => 'text',
						'eval' 					=> array('mandatory'=>true,'style'=>'width:380px')
					),
				),
			),
			'sql'					 => "text NULL"
		),	
		'preparation' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['preparation'],
			'exclude'                 => false,
			'search'                  => true,
			'filter'                  => false,
			'inputType'               => 'textarea',
			'eval'                    => array('mandatory'=>false, 'cols'=>'10','rows'=>'10','rte'=>'tinyMCE'),
			'sql'					=> "blob NULL"

		),
		'addImage' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['addImage'],
			'exclude'                 => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'singleSRC' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['singleSRC'],
			'exclude'                 => true,
			'inputType'               => 'fileTree',
			'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr','extensions' => 'jpg,jpeg,gif,png'),
			'sql'                     => "binary(16) NULL"
		),
		'alt' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['alt'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'title' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['title'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'published' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['published'],
			'exclude'                 => true,
			'filter'                  => true,
			'flag'                    => 2,
			'inputType'               => 'checkbox',
			'eval'                    => array('doNotCopy'=>true,'tl_class='=>'clr'),
			'sql'					=> "char(1) NOT NULL default ''"
		),
        'start' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['start'],
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 8,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'load_callback' => array
            (
                array('tl_simple_recipes_entries', 'loadDate')
            ),
            'sql'                     => "varchar(10) NOT NULL default '0'"
        ),
        'stop' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['stop'],
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 8,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                     => "varchar(10) NOT NULL default '0'"
        )
	)
);


/**
 * Class tl_simple_recipes_entries
 */
class tl_simple_recipes_entries extends Backend
{
	
	/**
	 * Autogenerate an article alias if it has not been set yet
	 * @param mixed
	 * @param object
	 * @return string
	 */
	public function generateAlias($varValue, DataContainer $dc)
	{
		$autoAlias = false;

		// Generate alias if there is none
		if (!strlen($varValue))
		{
			$autoAlias = true;
			$varValue = standardize($dc->activeRecord->headline);
		}

		$objAlias = $this->Database->prepare("SELECT id FROM tl_simple_recipes_entries WHERE id=? OR alias=?")
								   ->execute($dc->id, $varValue);

		// Check whether the page alias exists
		if ($objAlias->numRows > 1)
		{
			if (!$autoAlias)
			{
				throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
			}

			$varValue .= '-' . $dc->id;
		}

		return $varValue;
	}

    /**
     * List a particular record
     * @param $arrRow array
     * @return string
     */
    public function listEntries($arrRow)
    {
        $return = '<strong>'.$arrRow['headline'].'</strong> ';
        if((int) $arrRow['start'] > 0) $return .= date('d.m.Y',$arrRow['start']);
        if(((int) $arrRow['start'] > 0) && ((int) $arrRow['stop'] > 0)) $return .= ' - ';
        if((int) $arrRow['stop'] > 0) $return .= date('d.m.Y',$arrRow['stop']);

        return $return;
    }

    /**
     * Set the timestamp to 00:00:00 (see #26)
     *
     * @param integer $value
     *
     * @return integer
     */
    public function loadDate($value)
    {
        return strtotime(date('Y-m-d', $value) . ' 00:00:00');
    }
}
