<?php
$GLOBALS['TL_LANG']['tl_module']['item_template'] = array('Eintrag-Template', '');
$GLOBALS['TL_LANG']['tl_module']['archive_mode'] = array('zeige alle veröffentlichten der Vergangenheit','Es werden die Start und Stop-Angaben ignoriert, wenn zum Beispiel eine Archiv-Übersicht über alle Einträge in der Vergangenheit angezeigt werden soll.');