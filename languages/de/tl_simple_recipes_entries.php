<?php
/**
 * TL_ROOT/system/modules/simple_recipes/languages/de/tl_simple_recipes_entries.php
 *
 * Contao extension: simple_recipes
 * Deutsch translation file
 *
 * Copyright : &copy; Sven Rhinow <sven@sr-tag.de>
 * License   : LGPL
 * Author    : Sven Rhinow, http://www.sr-tag.de/
 * Translator: Sven Rhinow (scuM666)
 *
 */

/**
 * Global Operations
 */
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['properties'][0] = 'Einstellungen';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['properties'][1] = 'Alle Einstellungen zu den Rezepten';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['headline'][0]         = 'Rezept-Titel';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['headline'][1]         = 'geben sie den Titel für das Rezept an.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['main_ingredient'][0]         = 'Haupt-Zutat';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['main_ingredient'][1]         = 'Falls es eine Hauptzutat gibt geben sie diese hier an.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['alias'][0]			= 'Alias';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['alias'][1]        	= 'wird z.B. für die URL-Übergabe benötigt';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['categories'][0]			= 'Kategorien';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['categories'][1]        	= 'Ordnen Sie dem Rezept Kategorien zu.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['difficulty'][0]			= 'Schwierigkeit';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['difficulty'][1]        	= 'Geben Sie hier an wie schwierig es ist dieses Gericht herzustellen.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['number_of_people'][0]			= 'Anzahl der Personen';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['number_of_people'][1]        	= 'Geben Sie hier an für wieviel Leute die Mengenangaben des Gerichtes gedacht ist.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['ingredients'][0]			= 'Zutaten';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['ingredients'][1]        	= 'Geben Sie hier die Zutaten an (getrennt in z.B.  100 | g | Zucker).';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['addImage'][0]			= 'Rezeptbild anlegen';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['addImage'][1]        	= 'Geben Sie hier an ob sie ein Rezeptbild dem Rezept zuweisen möchten.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['singleSRC'][0]			= 'Rezeptbild';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['singleSRC'][1]        	= 'wählen Sie hier da passende Bild zum Rezept.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['alt'][0]			= 'Alternativer Text';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['alt'][1]        	= 'wird angezeigt wenn das Bild nicht zur Verfügung steht.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['title'][0]			= 'Bild-Titel';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['title'][1]        	= 'erscheint als Tooltip wenn sich der Mauszeiger über dem Bild aufhält.';

$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['count'][0]			= 'Menge';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['count'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['unit'][0]			= 'Einheit (optional)';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['unit'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['ingredient'][0]			= 'Zutat';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['ingredient'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['preparation'][0]			= 'Zubereitung';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['preparation'][1]        	= 'Geben sie hier ein wie das Rezept angewendet werden sollte.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['published'][0]			= 'Für Website-Besucher sichtbar';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['published'][1]        	= 'Geben sie hier ob das Rezept auf der Website angezeit werden soll.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['start']			= ['sichtbar ab','Wenn es erst ab einem bestimmten Tag sichtbar sein soll.'];
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['stop']        	= ['sichtbar bis','Wenn es nur bis zu einem bestimmten Tag sichtbar sein soll'];

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['new'][0]                          = 'Neues Rezept';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['new'][1]                          = 'Eine neues Rezept anlegen.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['edit'][0]                         = 'Rezept bearbeiten';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['edit'][1]                         = 'Rezept ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['copy'][0]                         = 'Rezept duplizieren';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['copy'][1]                         = 'Rezept ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['delete'][0]                       = 'Rezept löschen';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['delete'][1]                       = 'Rezept ID %s löschen.';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['show'][0]                         = 'Rezeptdetails anzeigen';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['show'][1]                         = 'Details für Rezept ID %s anzeigen.';

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['meta_legend']        	= 'Grund-Informationen';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['recipes_text']        	= 'Rezept';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['image_legend']        	= 'Bild zum Gericht';
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['general_legend']        	= 'weitere Einstellungen';

/**
 * Options
 */
$GLOBALS['TL_LANG']['tl_simple_recipes_entries']['difficulty_options'] = array(1 => 'sehr einfach', 2 => 'einfach', 3 => 'normal', 4 => 'schwer', 5 => 'sehr schwer');

