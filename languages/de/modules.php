<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2016 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['simple_recipes'] = array('Rezepte', 'Die einfache Rezept-Verwaltung');
$GLOBALS['TL_LANG']['MOD']['simple_recipes_entries'] = array('Rezept-Einträge', 'Verwaltung der Rezepte.');
$GLOBALS['TL_LANG']['MOD']['simple_recipes_categories'] = array('Rezept-Kategorien', 'Verwaltung der Rezept-Kategorien.');
