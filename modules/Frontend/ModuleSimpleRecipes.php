<?php
namespace Srhinow\SimpleRecipes\Modules\Frontend;

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */

use Contao\FrontendTemplate;
use Contao\Module;
use Contao\PageModel;
use Contao\StringUtil;


/**
 * Class ModuleSimpleRecipes
 */
abstract class ModuleSimpleRecipes extends Module
{
	/**
	 * Parse an item and return it as string
	 * @param object
	 * @param string
	 * @param integer
	 * @return string
	 */
	protected function parseRecipe($objRecipe, $strClass='', $intCount=0)
	{
		global $objPage;

		$objTemplate = new FrontendTemplate($this->item_template);
		$objTemplate->setData($objRecipe->row());
		$objTemplate->class = (($objRecipe->cssClass != '') ? ' ' . $objRecipe->cssClass : '') . $strClass;

        // Add an image
        if ($objRecipe->addImage && $objRecipe->singleSRC != '')
        {
            $objModel = \FilesModel::findByUuid($objRecipe->singleSRC);

            if ($objModel === null)
            {
                if (!\Validator::isUuid($objRecipe->singleSRC))
                {
                    $objTemplate->text = '<p class="error">'.$GLOBALS['TL_LANG']['ERR']['version2format'].'</p>';
                }
            }
            elseif (is_file(TL_ROOT . '/' . $objModel->path))
            {
                // Do not override the field now that we have a model registry (see #6303)
                $arrRecipe = $objRecipe->row();

                // Override the default image size
                if ($this->imgSize != '')
                {
                    $size = deserialize($this->imgSize);

                    if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2]))
                    {
                        $arrRecipe['size'] = $this->imgSize;
                    }
                }

                $arrRecipe['singleSRC'] = $objModel->path;
                $this->addImageToTemplate($objTemplate, $arrRecipe);
            }
        }

		//Detail-Url
		if($this->jumpTo)
		{
			$objDetailPage = PageModel::findByPk($this->jumpTo);
			$objTemplate->detailUrl = ampersand( $this->generateFrontendUrl($objDetailPage->row(),'/'.$objRecipe->alias) );
			$objTemplate->teaser = StringUtil::substr($objRecipe->preparation,100);
		}


        $objTemplate->ingredients = unserialize($objRecipe->ingredients);

		return $objTemplate->parse();
	}


	/**
	 * Parse one or more items and return them as array
	 * @param object
	 * @return array
	 */
	protected function parseRecipes($objRecipes)
	{
		$limit = $objRecipes->count();

		if ($limit < 1)
		{
			return array();
		}

		$count = 0;
		$arrRecipes = array();

		while ($objRecipes->next())
		{
			$arrRecipes[] = array
			(
				'data' => $objRecipes->row(),
				'html' => $this->parseRecipe($objRecipes, ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even'), $count)
			);
		}

		ksort($arrRecipes);
		return $arrRecipes;
	}
}
