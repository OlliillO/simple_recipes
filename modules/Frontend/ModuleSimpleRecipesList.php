<?php
namespace Srhinow\SimpleRecipes\Modules\Frontend;
/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

/**
 * Run in a custom namespace, so the class can be replaced
 */

use Contao\FrontendTemplate;
use Contao\Input;
use \Srhinow\SimpleRecipes\Models\SimpleRecipesEntriesModel;

/**
 * Class ModuleSimpleRecipesList
 *
 * Front end module "simple_recipes_list"
 */
class ModuleSimpleRecipesList extends ModuleSimpleRecipes
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_simple_recipes_list';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new \BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### REZEPT-LISTE ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		// Set the item from the auto_item parameter
		if (!isset($_GET['s']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
		{
			Input::setGet('s', \Input::get('auto_item'));
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{

		$offset = intval($this->skipFirst);
		$limit = null;
		$this->Template->recipes = array();

		// Maximum number of items
		if ($this->numberOfItems > 0)
		{
			$limit = $this->numberOfItems;
		}

		// Get the total number of items
		$intTotal = ($this->archive_mode == 1) ? SimpleRecipesEntriesModel::countArchiveRecipesByOpen() : SimpleRecipesEntriesModel::countRecipesByOpen();
//        print_r($intTotal); exit();
		// Filter anwenden um die Gesamtanzahl zuermitteln
		if($intTotal > 0)
		{
            $filterObj = ($this->archive_mode) ? SimpleRecipesEntriesModel::findArchiveRecipes($intTotal, 0) : SimpleRecipesEntriesModel::findRecipes($intTotal, 0);

			$counter = 0;
			$idArr = array();

			while($filterObj->next())
			{
				//wenn alle Filter stimmen -> Werte setzen
				$idArr[] = $filterObj->id;
				$counter++;
			}
			if((int) $intTotal > $counter) $intTotal = $counter;
		}

		if ((int) $intTotal < 1)
		{
			$this->Template = new FrontendTemplate('mod_simple_recipes_entries_empty');
			$this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyBmList'];
			return;
		}

		$total = $intTotal - $offset;

		// Split the results
		if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
		{

			// Adjust the overall limit
			if (isset($limit))
			{
				$total = min($limit, $total);
			}

			// Get the current page
			$id = 'page_n' . $this->id;
			$page = \Input::get($id) ?: 1;

			// Do not index or cache the page if the page number is outside the range
			if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
			{
				global $objPage;

				$objPage->noSearch = 1;
				$objPage->cache = 0;

				$objTarget = \PageModel::findByPk($objPage->id);
				if ($objTarget !== null)
				{
					$reloadUrl = ampersand($this->generateFrontendUrl( $objTarget->row() ) );
				}

				$this->redirect($reloadUrl);
			}

			// Set limit and offset
			$limit = $this->perPage;
			$offset += (max($page, 1) - 1) * $this->perPage;
			$skip = intval($this->skipFirst);

			// Overall limit
			if ($offset + $limit > $total + $skip)
			{
				$limit = $total + $skip - $offset;
			}

			// Add the pagination menu
			$objPagination = new \Pagination($total, $this->perPage, $GLOBALS['TL_CONFIG']['maxPaginationLinks'], $id);
			$this->Template->pagination = $objPagination->generate("\n  ");
		}

		// Get the items
		if($this->archive_mode)
		{
			$recipesObj = SimpleRecipesEntriesModel::findArchiveRecipes((int) $limit, $offset, $idArr);
		}
		else
		{
			$recipesObj = SimpleRecipesEntriesModel::findRecipes((int) $limit, $offset, $idArr);
		}

		// No items found
		if ($recipesObj === null)
		{
			$this->Template = new FrontendTemplate('mod_simple_recipes_entries_empty');
			$this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptySimpleRecipesList'];
		}
		else
		{
			$this->Template->recipes = $this->parseRecipes($recipesObj);
		}

		$this->Template->totalItems = $intTotal;
	}
}
